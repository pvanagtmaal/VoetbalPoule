from django.contrib.auth.models import AbstractUser
from django.db import models
from django.utils.translation import ugettext_lazy as _


class User(AbstractUser):
    pwd_changed = models.BooleanField(
        default=False, verbose_name=_('Changed initial password'))


class Competitor(models.Model):
    user = models.OneToOneField('User', on_delete=models.CASCADE, verbose_name=_('user'))
    competition = models.ForeignKey('competition.Competition', on_delete=models.CASCADE,
                                    default=None, null=True, blank=True,
                                    verbose_name=_('competition'))
    manager = models.ForeignKey('User', limit_choices_to={'is_staff': True}, related_name='manager',
                                on_delete=models.CASCADE, verbose_name=_('manager'))
    score = models.PositiveSmallIntegerField(default=0, verbose_name=_('score'))

    def __str__(self):
        return self.user.get_full_name()

    class Meta:
        verbose_name = _('competitor')
        verbose_name_plural = _('competitors')
