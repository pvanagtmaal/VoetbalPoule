from django.test import TestCase, Client
from django.urls import reverse

from competition.models import Competition
from users.models import User, Competitor


def create_manager(first_name='test', last_name='user', email='manager@address.com',
                   password='1234'):
    return User.objects.create_user(username=email, first_name=first_name, last_name=last_name,
                                    email=email, password=password, is_staff=True)


def create_simple_user(first_name='test', last_name='user', email='email@address.com',
                       password='1234'):
    return User.objects.create_user(username=email, first_name=first_name, last_name=last_name,
                                    email=email, password=password)


def create_user_with_changed_pwd(first_name='test2', last_name='user', email='email2@address.com',
                                 password='1234'):
    user = create_simple_user(first_name, last_name, email, password)
    user.pwd_changed = True
    user.save()
    return user


class TestUser(TestCase):
    def setUp(self):
        manager = create_manager(email='manager@test.nl')
        create_simple_user(email='test@test.nl')
        create_user_with_changed_pwd(email='test2@test.nl')
        Competition.objects.create(name='testcompetition', manager=manager)

    def test_pwd_changed_false_initially(self):
        user = User.objects.get(email='test@test.nl')
        self.assertFalse(user.pwd_changed)

    def test_create_manager(self):
        # Obvious test: check if the manually created user is staff
        self.assertTrue(User.objects.get(email='manager@test.nl').is_staff)

        c = Client()
        response = c.post(reverse('register-manager'), {
            'first_name': 'manager2',
            'last_name': 'test',
            'password': '1234',
            'password_verification': '1234',
            'email': 'manager2@test.nl',
        })

        # Assert the user is redirected to the dashboard and that he is staff
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response.url, '/')
        self.assertTrue(User.objects.get(email='manager2@test.nl').is_staff)

    def test_user_needs_to_change_password_after_login(self):
        c = Client()
        c.post('/login/', {'username': 'test@test.nl', 'password': '1234'})

        # Make sure the user is redirected to password change form
        response = c.get('/')
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response.url, reverse('first-login'))

    def test_competitor_created(self):
        c = Client()
        c.login(username='manager@test.nl', password='1234')
        competition = Competition.objects.get(name='testcompetition')

        response = c.post(reverse('create-regular-user'), {
            'first_name': 'testt',
            'last_name': 'testt',
            'password': '1234',
            'password_verification': '1234',
            'email': 'testt@testt.nl',
            'competition': competition.pk,
        })

        self.assertEqual(response.status_code, 302)

        # Assert the competitor has been created
        user = User.objects.get(email='testt@testt.nl')
        self.assertTrue(Competitor.objects.filter(user=user).exists())

        # Assert the user can access the dashboard
        User.objects.filter(email='testt@testt.nl').update(pwd_changed=True)
        c.login(username='testt@testt.nl', password='1234')
        response = c.get('/')
        self.assertEqual(response.status_code, 200)
