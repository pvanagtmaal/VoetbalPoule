from django import forms
from django.utils.translation import ugettext_lazy as _

from competition.models import Competition


def check_matching_passwords(data, password_name='password',
                             verification_name='password_verification'):
        password = data.get(password_name)
        password_verification = data.get(verification_name)

        if password != password_verification:
            raise forms.ValidationError(_('Passwords don\'t match!'))


class RegisterForm(forms.Form):
    first_name = forms.CharField(max_length=32)
    last_name = forms.CharField(max_length=64)
    email = forms.EmailField()
    password = forms.CharField(widget=forms.PasswordInput)
    password_verification = forms.CharField(widget=forms.PasswordInput)

    def clean(self):
        cleaned_data = super().clean()
        check_matching_passwords(cleaned_data)


class UserCreationForm(RegisterForm):
    competition = forms.ModelChoiceField(queryset=None)

    def __init__(self, manager, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.fields['competition'].queryset = Competition.objects.filter(manager=manager)


class PasswordModificationForm(forms.Form):
    password = forms.CharField(widget=forms.PasswordInput)
    password_verification = forms.CharField(widget=forms.PasswordInput)

    def clean(self):
        cleaned_data = super().clean()
        check_matching_passwords(cleaned_data)
