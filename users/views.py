from datetime import date, timedelta

from django.contrib.auth.decorators import login_required, user_passes_test
from django.db import IntegrityError
from django.shortcuts import render, redirect
from django.utils.translation import ugettext_lazy as _

from competition.models import Competition
from match.models import Match, Prediction
from users.forms import RegisterForm, PasswordModificationForm, UserCreationForm
from users.models import User, Competitor


def register(request):
    if request.method == 'POST':
        form = RegisterForm(request.POST)
        if form.is_valid():
            first_name = form.cleaned_data.get('first_name')
            last_name = form.cleaned_data.get('last_name')
            email = form.cleaned_data.get('email')
            password = form.cleaned_data.get('password')
            try:
                User.objects.create_user(
                    # Use email as username
                    email, email, password, is_staff=True,
                    first_name=first_name, last_name=last_name)
                return redirect('/')
            except IntegrityError:
                return render(request, 'registration/register.html', {
                    'form': form,
                    'error': _('The email address is already in use!')
                })

    form = RegisterForm()
    return render(request, 'registration/register.html', {'form': form})


@login_required
def dashboard(request):
    if request.user.is_staff:
        return render(request, 'dashboard/manager.html')
    elif request.user.pwd_changed:
        competitions = Competition.objects.filter(competitor__user=request.user).all()
        competitor = Competitor.objects.get(user=request.user)
        upcoming_date = date.today() + timedelta(days=1)
        upcoming = Match.objects.filter(competition=competitor.competition, date__gt=upcoming_date)
        predictions = Prediction.objects.filter(competitor=competitor)
        prediction_mapping = dict()
        for match in upcoming:
            if predictions.filter(match=match).exists():
                prediction_mapping[match] = predictions.get(match=match).prediction
            else:
                prediction_mapping[match] = None
        return render(request, 'dashboard/user.html', {
            'competitions': competitions,
            'competitor': competitor,
            'predictions': predictions,
            'prediction_mapping': prediction_mapping,
        })
    return redirect('/profile/first-login/')


@login_required
def first_login(request):
    if request.method == 'POST':
        form = PasswordModificationForm(request.POST)
        if form.is_valid():
            user = request.user
            user.set_password(form.cleaned_data.get('password'))
            user.pwd_changed = True
            user.save()
            return redirect('/')

    form = PasswordModificationForm()
    return render(request, 'registration/first_login.html', {'form': form})


@login_required
@user_passes_test(lambda u: u.is_staff)
def create_regular_user(request):
    if request.method == 'POST':
        form = UserCreationForm(request.user, request.POST)
        if form.is_valid():
            first_name = form.cleaned_data.get('first_name')
            last_name = form.cleaned_data.get('last_name')
            email = form.cleaned_data.get('email')
            password = form.cleaned_data.get('password')
            competition = form.cleaned_data.get('competition')

            try:
                user = User.objects.create_user(username=email, email=email, first_name=first_name,
                                                last_name=last_name, password=password)
                Competitor.objects.update_or_create(manager=request.user, user=user,
                                                    defaults={'competition': competition})
                return redirect('/')
            except IntegrityError:
                return render(request, 'registration/create_regular_user.html', {
                    'form': form,
                    'error': _('The email address is already in use!')
                })

    form = UserCreationForm(request.user)
    return render(request, 'registration/create_regular_user.html', {'form': form})
