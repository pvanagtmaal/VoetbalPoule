from django.urls import path

from users import views


urlpatterns = [
    path('', views.dashboard),
    path('profile/create/', views.create_regular_user, name='create-regular-user'),
    path('profile/first-login/', views.first_login, name='first-login'),
    path('profile/register/', views.register, name='register-manager'),
]
