from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.decorators import login_required, user_passes_test
from django.shortcuts import render, redirect

from team.forms import TeamCreationForm
from team.models import Team


@login_required
@user_passes_test(lambda u: u.is_staff)
def add_team(request):
    if request.method == 'POST':
        form = TeamCreationForm(request.POST)
        if form.is_valid():
            team, created = Team.objects.get_or_create(name=form.cleaned_data.get('name'))

            if created:
                return redirect('/')
            else:
                return render(request, 'team/add_team.html', {
                    'form': form,
                    'error': _('A team with that name already exists')
                })

    form = TeamCreationForm()
    return render(request, 'team/add_team.html', {'form': form})
