from django.test import TestCase, Client
from django.urls import reverse

from users.models import User


class TeamTestCase(TestCase):
    def setUp(self):
        User.objects.create_user(username='manager@test.nl', password='1234', is_staff=True)

    def test_add_team(self):
        c = Client()
        c.login(username='manager@test.nl', password='1234')

        response = c.post(reverse('add-team'), {'name': 'testteam'})
        # Assert redirect to dashboard
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response.url, '/')

    def test_get_add_team(self):
        c = Client()
        c.login(username='manager@test.nl', password='1234')

        response = c.get(reverse('add-team'))
        self.assertTemplateUsed(response, 'team/add_team.html')

    def test_add_existing_team(self):
        c = Client()
        c.login(username='manager@test.nl', password='1234')

        c.post(reverse('add-team'), {'name': 'testteam'})
        response = c.post(reverse('add-team'), {'name': 'testteam'})
        self.assertIsNotNone(response.context.get('error', None))
