from django.urls import path

from team import views

urlpatterns = [
    path('add/', views.add_team, name='add-team'),
]
