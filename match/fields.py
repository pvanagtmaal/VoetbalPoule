from django import forms
from django.core.validators import MaxValueValidator
from django.db import models
from django.db.models import PositiveSmallIntegerField
from django.utils.translation import ugettext_lazy as _


class ScoreModelField(models.CharField):
    description = _('Score up to 999-999')

    def __init__(self, *args, **kwargs):
        kwargs['max_length'] = 5
        kwargs['default'] = None
        kwargs['null'] = True
        kwargs['blank'] = True
        super().__init__(*args, **kwargs)

    def deconstruct(self):
        name, path, args, kwargs = super().deconstruct()
        del kwargs['max_length']
        del kwargs['default']
        del kwargs['null']
        del kwargs['blank']
        return name, path, args, kwargs


class ResultFormField(forms.MultiValueField):
    def __init__(self, **kwargs):
        fields = (
            PositiveSmallIntegerField(default=0, validators=[MaxValueValidator(999)]),
            PositiveSmallIntegerField(default=0, validators=[MaxValueValidator(999)]),
        )

        super().__init__(fields=fields, required=False, **kwargs)

    def compress(self, data_list):
        if len(data_list) != 2:
            return '0-0'
        return '{}-{}'.format(data_list[0], data_list[1])


class ScoreWidget(forms.MultiWidget):
    def __init__(self):
        widgets = (
            forms.widgets.NumberInput(), forms.widgets.NumberInput()
        )

        super().__init__(widgets=widgets)

    def decompress(self, value):
        if value:
            return value.split('-')
        return [0, 0]
