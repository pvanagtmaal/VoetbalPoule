from django import forms

from match import fields


class SchedulerForm(forms.Form):
    team_a = forms.ModelChoiceField(queryset=None)
    team_b = forms.ModelChoiceField(queryset=None)
    date = forms.DateTimeField()
    result = fields.ResultFormField()

    def __init__(self, competition, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.fields['team_a'].queryset = competition.teams
        self.fields['team_b'].queryset = competition.teams
