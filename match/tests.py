from django.http import HttpResponseNotAllowed
from django.test import TestCase, Client
from django.urls import reverse

from competition.models import Competition
from match.models import Match, Prediction
from team.models import Team
from users.models import User, Competitor


class MatchTestCase(TestCase):
    def setUp(self):
        manager = User.objects.create_user(username='manager', password='1234', is_staff=True)
        user = User.objects.create_user(username='regular', password='1234')
        user2 = User.objects.create_user(username='regular2', password='1234')
        competition = Competition.objects.create(name='competition', manager=manager)
        Competitor.objects.create(user=user, competition=competition, manager=manager)
        Competitor.objects.create(user=user2, competition=competition, manager=manager)

        team_a = Team.objects.create(name='team_a')
        team_b = Team.objects.create(name='team_b')
        Match.objects.create(competition=competition, team_a=team_a, team_b=team_b,
                             date='2018-10-20')

    def test_schedule_match(self):
        c = Client()
        c.login(username='manager', password='1234')

        competition = Competition.objects.get(name='competition')
        team_a = Team.objects.get(name='team_a')
        team_b = Team.objects.get(name='team_b')

        response = c.get(reverse('schedule-match'))
        self.assertIsInstance(response, HttpResponseNotAllowed)

        response = c.post(reverse('schedule-match'), {
            'team_a': team_a.pk,
            'team_b': team_b.pk,
            'date': '2018-10-21',
            'competition': competition.pk
        })

        self.assertEqual(response.status_code, 302)

        match = Match.objects.filter(team_a=team_a, team_b=team_b, date='2018-10-21')
        self.assertQuerysetEqual(competition.matches.filter(date='2018-10-21'), match)

    def test_update_match(self):
        c = Client()
        c.login(username='manager', password='1234')

        competition = Competition.objects.get(name='competition')
        match = Match.objects.get(date='2018-10-20')

        response = c.get(reverse('update-match', kwargs={
            'competition_id': competition.pk, 'match_id': match.pk
        }))
        self.assertIsInstance(response, HttpResponseNotAllowed)

        response = c.post(reverse('update-match', kwargs={
            'competition_id': competition.pk, 'match_id': match.pk
        }), {'result': '1-0'})

        self.assertEqual(response.status_code, 302)
        self.assertEqual(Match.objects.get(date='2018-10-20').result, '1-0')

    def test_predict_match(self):
        c = Client()
        c.login(username='regular', password='1234')

        match = Match.objects.get(date='2018-10-20')

        response = c.post(reverse('predict-match', kwargs={'match_id': match.pk}), {
            'prediction': '1-0'
        })

        self.assertRedirects(response, '/', target_status_code=302)
        self.assertTrue(Prediction.objects.filter(competitor__user__username='regular').exists())

    def test_update_scores_for_prediction(self):
        c = Client()
        c.login(username='manager', password='1234')

        competitor = Competitor.objects.get(user__username='regular')
        competitor2 = Competitor.objects.get(user__username='regular2')
        competition = Competition.objects.get(name='competition')
        match = Match.objects.get(date='2018-10-20')

        # make prediction
        Prediction.objects.create(match=match, competitor=competitor, prediction='1-0')
        Prediction.objects.create(match=match, competitor=competitor2, prediction='0-0')

        # Assert user has score 0 before we start
        self.assertEqual(0, competitor.score)
        self.assertEqual(0, competitor2.score)

        c.post(reverse('update-match', kwargs={
            'competition_id': competition.pk, 'match_id': match.pk
        }), {'result': '1-0'})

        # Test correct prediction increments score
        self.assertEqual(1, Competitor.objects.get(pk=competitor.pk).score)

        # Test incorrect prediction leaves score alone
        self.assertEqual(0, Competitor.objects.get(pk=competitor2.pk).score)

    def test_manage_matches(self):
        c = Client()
        c.login(username='manager', password='1234')

        # Test without competition
        response = c.get(reverse('manage-matches'))
        self.assertRedirects(response, '/')

        # Test with competition
        competition = Competition.objects.get(name='competition')
        response = c.get(reverse('manage-matches') + '?competition_id={}'.format(competition.pk))
        self.assertTemplateUsed(response, 'match/manage_matches.html')
