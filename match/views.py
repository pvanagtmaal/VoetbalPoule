from django.contrib.auth.decorators import login_required, user_passes_test
from django.db import IntegrityError
from django.shortcuts import render, redirect
from django.urls import reverse
from django.views.decorators.http import require_http_methods

from competition.models import Competition
from match.forms import SchedulerForm
from match.models import Match, Prediction


@login_required
@user_passes_test(lambda u: u.is_staff)
@require_http_methods(['POST'])
def schedule_match(request):
    try:
        competition = Competition.objects.get(pk=request.POST.get('competition', -1))
    except Competition.DoesNotExist:
        # Do something
        competition = None

    form = SchedulerForm(competition, request.POST)
    if form.is_valid():
        team_a = form.cleaned_data.get('team_a')
        team_b = form.cleaned_data.get('team_b')
        date = form.cleaned_data.get('date')

        try:
            match = Match.objects.create(
                team_a=team_a, team_b=team_b, date=date, competition=competition)
            if competition:
                competition.matches.add(match)
        except IntegrityError as e:
            # Do something
            print('Could not add match due to a DB restriction: {}'.format(e))

    return redirect(reverse('manage-matches') + '?competition_id={}'.format(competition.pk))


@login_required
@user_passes_test(lambda u: u.is_staff)
@require_http_methods(['POST'])
def update_match(request, competition_id, match_id):
    try:
        match = Match.objects.get(pk=match_id)
        match.result = request.POST.get('result')
        match.save()

        # Now update all scores
        for prediction in match.prediction_set.all():
            if prediction.prediction == match.result:
                prediction.competitor.score += 1
                prediction.competitor.save()
    except Match.DoesNotExist:
        # Do something
        print('Could not update match score because the match was not found')
    return redirect(reverse('manage-matches') + '?competition_id={}'.format(competition_id))


@login_required
@require_http_methods(['POST'])
def predict_match(request, match_id):
    try:
        match = Match.objects.get(pk=match_id)
        competitor = request.user.competitor
        prediction = request.POST.get('prediction')
        Prediction.objects.create(competitor=competitor, match=match, prediction=prediction)
    except Match.DoesNotExist:
        # Do something
        print('Match does not exist')
    except AttributeError:
        # Do something
        print('User is not a competitor???')

    return redirect('/')


@login_required
@user_passes_test(lambda u: u.is_staff)
def manage_matches(request):
    try:
        competition = Competition.objects.get(pk=request.GET.get('competition_id', -1))
        form = SchedulerForm(competition)
        return render(request, 'match/manage_matches.html', {
            'competition': competition,
            'form': form
        })
    except Competition.DoesNotExist:
        return redirect('/')
