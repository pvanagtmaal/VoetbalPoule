from django.urls import path

from match import views

urlpatterns = [
    path('manage/', views.manage_matches, name='manage-matches'),
    path('schedule/', views.schedule_match, name='schedule-match'),
    path('<int:competition_id>/<int:match_id>/update/', views.update_match, name='update-match'),
    path('<int:match_id>/predict/', views.predict_match, name='predict-match'),
]
