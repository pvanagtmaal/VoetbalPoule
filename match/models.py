from django.db import models
from django.utils.translation import ugettext_lazy as _

from datetime import datetime

from match import fields
from match.fields import ScoreModelField


class Match(models.Model):
    team_a = models.ForeignKey('team.Team', on_delete=models.DO_NOTHING,
                               related_name='team_a', verbose_name=_('team A'))
    team_b = models.ForeignKey('team.Team', on_delete=models.DO_NOTHING,
                               related_name='team_b', verbose_name=_('team B'))
    date = models.DateTimeField(verbose_name=_('date'))
    result = ScoreModelField(default=None, blank=True, null=True)

    @property
    def is_played(self):
        return datetime.now().timestamp() > self.date.timestamp()

    def __str__(self):
        return '{} - {}'.format(self.team_a, self.team_b)

    class Meta:
        verbose_name = _('match')
        verbose_name_plural = _('matches')
        unique_together = ('team_a', 'team_b', 'date')


class Prediction(models.Model):
    competitor = models.ForeignKey('users.Competitor', on_delete=models.CASCADE,
                                   verbose_name=_('competitor'))
    match = models.ForeignKey('match.Match', on_delete=models.CASCADE, verbose_name=_('match'))
    prediction = fields.ScoreModelField()

    def __str__(self):
        return '{}: {}'.format(self.match, self.prediction)

    class Meta:
        unique_together = ('competitor', 'match')
        verbose_name = _('prediction')
        verbose_name_plural = _('predictions')
