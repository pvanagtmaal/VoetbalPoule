from django.urls import path

from competition import views

urlpatterns = [
    path('add/', views.add_competition, name='add-competition'),
    path('manage/', views.manage_competitions, name='manage-competitions'),
    path('<int:competition_id>/update-competitors/', views.update_competitors,
         name='update-competitors'),
    path('<int:competition_id>/update-teams/', views.update_teams,
         name='update-teams'),
    path('delete/', views.delete_competition, name='delete-competition'),
    path('leave/', views.leave_competition, name='leave-competition'),
]
