from django.db import models
from django.utils.translation import ugettext_lazy as _


class Competition(models.Model):
    name = models.CharField(max_length=64, verbose_name=_('name'))
    teams = models.ManyToManyField('team.Team', verbose_name=_('teams'))
    matches = models.ManyToManyField('match.Match', verbose_name=_('matches'))
    manager = models.ForeignKey('users.User', on_delete=models.CASCADE,
                                limit_choices_to={'is_staff': True})

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _('competition')
        verbose_name_plural = _('competitions')
