from django.test import TestCase, Client
from django.urls import reverse

from competition.models import Competition
from team.models import Team
from users.models import User, Competitor


class CompetitionTestCase(TestCase):
    def setUp(self):
        manager = User.objects.create_user(username='manager', password='1234', is_staff=True)
        user = User.objects.create_user(username='regular', password='1234')
        user2 = User.objects.create_user(username='regular2', password='1234')

        team_a = Team.objects.create(name='team_a')
        team_b = Team.objects.create(name='team_b')
        competition = Competition.objects.create(name='test', manager=manager)
        competition.teams.set((team_a, team_b))
        Competitor.objects.create(manager=manager, competition=competition, user=user)
        Competitor.objects.create(manager=manager, competition=competition, user=user2)

    def test_add_competition(self):
        c = Client()
        c.login(username='manager', password='1234')

        teams = Team.objects.all().values_list('pk', flat=True)

        response = c.get(reverse('add-competition'))
        self.assertTemplateUsed(response, 'competition/add_competition.html')

        response = c.post(reverse('add-competition'), {'name': 'competition', 'teams': teams})
        self.assertTrue(Competition.objects.get(name='competition'))
        self.assertRedirects(response, '/')

        response = c.post(reverse('add-competition'), {'name': 'competition', 'teams': teams})
        self.assertContains(response, 'already exists')

    def test_delete_competition(self):
        c = Client()
        c.login(username='manager', password='1234')

        competition = Competition.objects.get(name='test')
        c.post(reverse('delete-competition'), {'competition_id': competition.pk})
        self.assertFalse(Competition.objects.filter(name='test').exists())

    def test_leave_competition(self):
        c = Client()
        c.login(username='regular', password='1234')

        competitor = Competitor.objects.get(user__username='regular')
        c.post(reverse('leave-competition'), {'competition_id': competitor.competition_id})
        competitors = Competition.objects.get(pk=competitor.competition_id).competitor_set
        self.assertFalse(competitors.filter(pk=competitor.pk).exists())

    def test_manage_competitions(self):
        c = Client()
        c.login(username='manager', password='1234')

        response = c.get(reverse('manage-competitions'))
        self.assertTemplateUsed('competition/manage_competitions.html')
        self.assertEqual(200, response.status_code)

    def test_update_competitors(self):
        c = Client()
        c.login(username='manager', password='1234')

        competition = Competition.objects.get(name='test')

        self.assertEqual(competition.competitor_set.count(), 2)
        c.post(reverse('update-competitors', kwargs={'competition_id': competition.pk}), {
            'competitors[]': competition.competitor_set.first().pk,
        })
        self.assertEqual(Competition.objects.get(name='test').competitor_set.count(), 1)

    def test_update_teams(self):
        c = Client()
        c.login(username='manager', password='1234')

        competition = Competition.objects.get(name='test')
        teams = list(competition.teams.all().values_list('pk', flat=True))
        team_c = Team.objects.create(name='team_c')
        teams.append(team_c.pk)

        self.assertEqual(competition.teams.count(), 2)
        c.post(reverse('update-teams', kwargs={'competition_id': competition.pk}), {
            'teams[]': ','.join(str(t) for t in teams)
        })
        self.assertEqual(Competition.objects.get(name='test').teams.count(), 3)
