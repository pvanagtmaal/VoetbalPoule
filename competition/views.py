from django.contrib.auth.decorators import login_required, user_passes_test
from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.utils.translation import ugettext_lazy as _
from django.urls import reverse
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.http import require_http_methods

from competition.forms import CompetitionCreationForm
from competition.models import Competition
from team.models import Team
from users.models import Competitor


@login_required
@user_passes_test(lambda u: u.is_staff)
def add_competition(request):
    if request.method == 'POST':
        form = CompetitionCreationForm(request.POST)
        if form.is_valid():
            competition, created = Competition.objects.get_or_create(
                name=form.cleaned_data.get('name'), manager=request.user)
            competition.teams.set(form.cleaned_data.get('teams'))

            if created:
                return redirect('/')
            else:
                return render(request, 'competition/add_competition.html', {
                    'form': form,
                    'error': _('A competition with that name already exists')
                })

    form = CompetitionCreationForm()
    return render(request, 'competition/add_competition.html', {'form': form})


@login_required
@user_passes_test(lambda u: u.is_staff)
@require_http_methods(['POST'])
def delete_competition(request):
    Competition.objects.filter(pk=request.POST.get('competition_id'), manager=request.user).delete()
    return redirect(reverse('manage-competitions'))


@login_required
@user_passes_test(lambda u: not u.is_staff)
@require_http_methods(['POST'])
def leave_competition(request):
    Competitor.objects.filter(
        user=request.user, competition_id=request.POST.get('competition_id')).delete()
    return redirect('/')


@login_required
@user_passes_test(lambda u: u.is_staff)
@require_http_methods(['GET'])
def manage_competitions(request):
    competitions = Competition.objects.filter(manager=request.user)
    competitors = Competitor.objects.filter(manager=request.user).all()
    teams = Team.objects.all()

    return render(request, 'competition/manage_competitions.html', {
        'competitions': competitions,
        'competitors': competitors,
        'teams': teams,
    })


@login_required
@user_passes_test(lambda u: u.is_staff)
@require_http_methods(['POST'])
@csrf_exempt
def update_competitors(request, competition_id):
    competitor_ids = [int(c) for c in request.POST.get('competitors[]', '').split(',') if c]

    # First update existing competitors
    Competitor.objects.filter(competition_id=competition_id).update(competition_id=None)
    # Then set all current competitors
    Competitor.objects.filter(pk__in=competitor_ids).update(competition_id=competition_id)

    return HttpResponse()


@login_required
@user_passes_test(lambda u: u.is_staff)
@require_http_methods(['POST'])
@csrf_exempt
def update_teams(request, competition_id):
    team_ids = [int(c) for c in request.POST.get('teams[]', '').split(',') if c]
    competition = Competition.objects.get(pk=competition_id, manager=request.user)

    competition.teams.set(Team.objects.filter(pk__in=team_ids))
    competition.save()

    return HttpResponse()
