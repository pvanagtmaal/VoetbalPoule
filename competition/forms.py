from django import forms

from competition.models import Competition


class CompetitionCreationForm(forms.ModelForm):
    class Meta:
        model = Competition
        fields = ('name', 'teams',)
