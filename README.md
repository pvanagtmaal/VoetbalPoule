# VoetbalPoule
This is a Django project purely meant for demonstration purposes.

## Software
* Python 3.5+
* Django 2.0+
* MySQL (with mysqldbclient for Django)
* coverage for testing

## Installing the project
* Install the virtual environment
* Create the database and database user
* Run the migrations
* All set :)

## Running the tests
`coverage run --source='.' manage.py test`

To see coverage percentages:

`coverage report`

## Design decisions
In this section, the design decisions made for this project will be addressed.

### Class based versus function based views
I feel class based views are more of an unnecessary wrapper for function based views than an actual
feature, that obscures the code flow. Therefore, I like to use function based views in conjunction
with one or more decorators better. However, due to the repetitional logic for e.g. forms, I can
understand why one would use class-based views instead to avoid duplication.

### MySQL vs Sqlite3
The main reason to use MySQL over a simpler DB such as Sqlite3 is that it scales much better and
does not need to run on the same server.

### Django Authentication vs custom authentication
The manager and regular users subclass the generic User model from the auth app. This simplifies
authentication as the auth app contains some useful components such as views and decorators for
authentication.

### Custom score field versus two separate fields
Instead of simply adding two fields for storing match results I decided to go with a custom field
to store the result as a string. This simplifies comparision with predictions but does not
complicate entering results for the user.

## Additional notes
* Not all form handling is clean, e.g. there are some print statements where user feedback would
normally be given.
* Not all possible exceptions are caught so it is possible to generate a 500 error by manually
generating requests
* Absolutely no attention is paid to how the site looks. It is plain HTML without any CSS.
* Javascript is only added where without it the user flow would become vague.
* The views called through Ajax calls are not secure due to the `@csrf_excempt` decorator.
