from django.contrib.auth import views
from django.urls import path, include

urlpatterns = [
    path('', include('users.urls')),
    path('login/', views.login, name='login'),
    path('logout/', views.logout, name='logout'),
    path('competition/', include('competition.urls')),
    path('match/', include('match.urls')),
    path('team/', include('team.urls')),
]
